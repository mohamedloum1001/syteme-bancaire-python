import json
from datetime import datetime
from itertools import count

class Utilisateur:
    _id_user_counter = count(1)  
    _id_transaction_counter = count(1)  

    def __init__(self, nom, numero_compte, solde=0, compte_bloque=False):
        self._id = next(Utilisateur._id_user_counter)  
        self.nom = nom
        self.numero_compte = numero_compte
        self._solde = solde
        self._historique_transactions = []
        self._compte_bloque = compte_bloque

    def bloquer_compte(self):
        self._compte_bloque = True

    def debloquer_compte(self):
        self._compte_bloque = False

    def est_compte_bloque(self):
        return self._compte_bloque

    def consulter_solde(self):
        return self._solde

    def obtenir_historique_transactions(self):
        return self._historique_transactions

    def effectuer_transaction(self, type_transaction, montant, partenaire=None):
        if not self.est_compte_bloque() and (type_transaction == "depot" or (0 < montant <= self._solde and partenaire and not partenaire.est_compte_bloque())):
            if type_transaction == "depot":
                self._solde += montant
            elif type_transaction == "retrait":
                self._solde -= montant
            elif type_transaction == "transfert":
                self._solde -= montant
                partenaire._solde += montant

            horodatage = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            transaction = {"id_transaction": next(Utilisateur._id_transaction_counter), "horodatage": horodatage, "type": type_transaction, "montant": montant}
            self._historique_transactions.append(transaction)

            if partenaire:
                partenaire._historique_transactions.append(transaction)
                
            return True
        else:
            print("Opération non autorisée.")
            return False

    def afficher_historique(self):
        if self._historique_transactions:
            print(f"Historique des transactions pour {self.nom} (ID {self._id}):")
            for transaction in self._historique_transactions:
                print(f"ID Transaction: {transaction['id_transaction']}, Type: {transaction['type']}, Montant: {transaction['montant']}, Horodatage: {transaction['horodatage']}")
        else:
            print(f"Aucune transaction pour {self.nom}.")

class Admin(Utilisateur):
    def creer_compte(self):
        nom_client = input("Entrez le nom du client : ")
        numero_compte_client = input("Entrez le numéro de compte du client : ")

        while True:
            try:
                solde_initial = float(input("Entrez le solde initial : "))
                break
            except ValueError:
                print("Erreur : Veuillez entrer un montant valide (nombre entier ou à virgule flottante).")

        nouveau_client = Client(nom_client, numero_compte_client, solde_initial)
        return nouveau_client

def sauvegarder_dans_fichier(donnees, nom_fichier):
    try:
        with open(nom_fichier, 'w') as fichier:
            json.dump(donnees, fichier, default=lambda o: o.__dict__, indent=4)
    except Exception as e:
        print(f"Erreur lors de la sauvegarde dans le fichier : {e}")

def charger_depuis_fichier(nom_fichier):
    try:
        with open(nom_fichier, 'r') as fichier:
            donnees = json.load(fichier)
        return donnees
    except FileNotFoundError:
        return {"clients": [], "transactions": []}
    except Exception as e:
        print(f"Erreur lors du chargement depuis le fichier : {e}")
        return {"clients": [], "transactions": []}

class Client(Utilisateur):
    def effectuer_depot(self, montant):
        return self.effectuer_transaction("depot", montant)

    def effectuer_retrait(self, montant):
        return self.effectuer_transaction("retrait", montant)

    def effectuer_transfert(self, utilisateur_cible, montant):
        return self.effectuer_transaction("transfert", montant, utilisateur_cible)

def menu_admin():
    while True:
        print("\n1. Créer un Compte")
        print("2. Consulter l'Historique des Transactions")
        print("3. Bloquer un Compte")
        print("4. Débloquer un Compte")
        print("5. Supprimer un Compte")
        print("6. Sauvegarder et Quitter")
        
        choix = input("Entrez votre choix (1/2/3/4/5/6) : ")

        if choix == "1":
            nouveau_client = admin.creer_compte()
            if nouveau_client:
                clients_charges.append(nouveau_client)
                print(f"Nouveau compte créé pour {nouveau_client.nom} avec l'ID {nouveau_client._id}")

        
        elif choix == "2":
            numero_compte = input("Entrez le numéro de compte pour consulter l'historique des transactions : ")
            utilisateur = next((c for c in clients_charges if c.numero_compte == numero_compte), None)

            if utilisateur:
                utilisateur.afficher_historique()
            else:
                print("Utilisateur introuvable.")

        elif choix == "3":
            numero_compte_a_bloquer = input("Entrez le numéro de compte à bloquer : ")
            utilisateur_a_bloquer = next((c for c in clients_charges if c.numero_compte == numero_compte_a_bloquer), None)
            if utilisateur_a_bloquer:
                admin.bloquer_compte(utilisateur_a_bloquer)
                print(f"Compte de {utilisateur_a_bloquer.nom} bloqué.")
            else:
                print("Utilisateur introuvable.")

        elif choix == "4":
            numero_compte_a_debloquer = input("Entrez le numéro de compte à débloquer : ")
            utilisateur_a_debloquer = next((c for c in clients_charges if c.numero_compte == numero_compte_a_debloquer), None)
            if utilisateur_a_debloquer:
                admin.debloquer_compte(utilisateur_a_debloquer)
                print(f"Le compte de {utilisateur_a_debloquer.nom} a été débloqué avec succès.")
            else:
                print("Utilisateur introuvable.")

        elif choix == "5":
            numero_compte_a_supprimer = input("Entrez le numéro de compte à supprimer : ")
            utilisateur_a_supprimer = next((c for c in clients_charges if c.numero_compte == numero_compte_a_supprimer), None)
            if utilisateur_a_supprimer:
                admin.supprimer_compte(utilisateur_a_supprimer)
            else:
                print("Utilisateur introuvable.")

        elif choix == "6":
            donnees_a_sauvegarder = [client.__dict__ for client in clients_charges]
            sauvegarder_dans_fichier(donnees_a_sauvegarder, "donnees_bancaires.json")
            print("Données sauvegardées. Fin...")
            break

        else:
            print("Choix invalide. Veuillez entrer une option valide.")

def menu_client(client):
    while True:
        print("\n1. Effectuer une Transaction")
        print("2. Consulter l'Historique des Transactions")
        print("3. Sauvegarder et Quitter")
        
        choix = input("Entrez votre choix (1/2/3) : ")

        if choix == "1":
            montant = float(input("Entrez le montant de la transaction : "))
            type_transaction = input("Choisissez le type de transaction (depot/retrait/transfert) : ")

            if type_transaction == "depot":
                client.effectuer_depot(montant)
            elif type_transaction == "retrait":
                client.effectuer_retrait(montant)
            elif type_transaction == "transfert":
                numero_compte_cible = input("Entrez le numéro de compte de la cible : ")
                utilisateur_cible = next((c for c in clients_charges if c.numero_compte == numero_compte_cible), None)

                if utilisateur_cible:
                    client.effectuer_transfert(utilisateur_cible, montant)
                else:
                    print("Le client ciblé est introuvable.")
            else:
                print("Type de transaction invalide.")

            print(f"Solde de {client.nom} : {client.consulter_solde()}")
            print(f"Historique des transactions de {client.nom} : {client.obtenir_historique_transactions()}")

        elif choix == "2":
            client.afficher_historique()

        elif choix == "3":
            donnees_a_sauvegarder = [client.__dict__ for client in clients_charges]
            sauvegarder_dans_fichier(donnees_a_sauvegarder, "donnees_bancaires.json")
            print("Données sauvegardées. Fin...")
            break

        else:
            print("Choix invalide. Veuillez entrer une option valide.")

# Choix entre admin et client
utilisateur_type = input("Êtes-vous un admin ou un client? Entrez 'admin' ou 'client': ")

# Chargement des données depuis le fichier
donnees_chargees = charger_depuis_fichier("donnees_bancaires.json")
clients_charges = [Client(nom=donnees_client['nom'], numero_compte=donnees_client['numero_compte'], solde=donnees_client['_solde'], compte_bloque=donnees_client['_compte_bloque']) for donnees_client in donnees_chargees]

admin = Admin("Admin", "999999")

if utilisateur_type.lower() == "admin":
    menu_admin()
elif utilisateur_type.lower() == "client":
    numero_compte_client = input("Entrez votre numéro de compte : ")
    client = next((c for c in clients_charges if c.numero_compte == numero_compte_client), None)

    if client:
        menu_client(client)
    else:
        print("Utilisateur introuvable.")
else:
    print("Type d'utilisateur invalide.")
